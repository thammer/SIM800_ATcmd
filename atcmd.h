#ifndef __ATCMD_H__
#define __ATCMD_H__

#include "pdu.h"

#define AT_HEADER         "AT\r"

//---------AT��������ɹ����-----------------------------------
#define CMD_OK              "\r\nOK\r\n"
#define CMD_ERR            "\r\nERROR\r\n"
#define CMD_CONNECT         "\r\nCONNECT\r\n"
#define CMD_SAPBR           "+SAPBR"


#define CMD_SAVE_SUFFIX     ";&W"
#define CMD_SAVE                "AT&W\r"

/****************************************************************************************************
** ATE Set Command Echo Mode
**@Command format: ATE<value>
-----------------------------------------------------------------------------------------------------
Response
This setting determines whether or not the TA echoes characters received
from TE during Command state.
OK
-----------------------------------------------------------------------------------------------------
Parameter
<value> 
    0 Echo mode off
    1 Echo mode on
****************************************************************************************************/
#define DISABLE_ECHO    "ATE0\r"
#define ENABLE_ECHO     "ATE1\r"


/****************************************************************************************************
** list TE-TA Fixed Local Rate supported
**@Command format: AT+IPR=?
-----------------------------------------------------------------------------------------------------
Response
+IPR: (list of supported auto detectable <rate>s),(list of supported
fixed-only <rate>s)
OK
-----------------------------------------------------------------------------------------------------
** read TE-TA Fixed Local Rate
**@Command format: AT+IPR?
-----------------------------------------------------------------------------------------------------
Response
+IPR: <rate>
OK
-----------------------------------------------------------------------------------------------------
** write TE-TA Fixed Local Rate
**@Command format: AT+IPR=<rate>
-----------------------------------------------------------------------------------------------------
Response
This parameter setting determines the data rate of the TA on the serial
interface. The rate of Command takes effect following the issuance of any
result code associated with the current Command line.
OK
-----------------------------------------------------------------------------------------------------

****************************************************************************************************/
#define CMD_LIST_BR_SUPPORTED   "AT+IPR=?\r"
#define CMD_WR_BR   "AT+IPR="
#define CMD_RD_BR   "AT+IPR?\r"

/****************************************************************************************************
** Mobile Originated Call to Dial A Number
**@Command format: ATD<n>[<mgsm][;]
-----------------------------------------------------------------------------------------------------
Response
+IPR: (list of supported auto detectable <rate>s),(list of supported
fixed-only <rate>s)
OK
-----------------------------------------------------------------------------------------------------

****************************************************************************************************/

#define CMD_DAIL_CALL   "ATD"
#define CMD_HANGUP      "ATH\r"

/****************************************************************************************************
** Select SMS Message Format
**@Command format: 
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------

****************************************************************************************************/
#define CMD_LIST_MSG_FORMAT     "AT+CMGF=?\r"
#define CMD_RD_MSG_FORMAT       "AT+CMGF?\r"
#define CMD_WR_MSG_FORMAT_TEXT       "AT+CMGF=1\r"
#define CMD_WR_MSG_FORMAT_PDU       "AT+CMGF=0\r"

/****************************************************************************************************
** Send SMS Message
**@Command format: 
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------

****************************************************************************************************/
#define CMD_TEST_SMS     "AT+CMGS=?\r"
#define CMD_SEND_SMS       "AT+CMGS="
#define CMD_SMS_INPUT   ">"
#define CMD_SMS_RECV_SM    "+CMTI: \"SM\""
#define CMD_SMS_RECV_MT    "+CMTI: \"MT\""
#define CMD_SMS_RECV_ME    "+CMTI: \"ME\""


#define SMS_ASCII_NUM   140

/****************************************************************************************************
** Receive SMS Message
**@Command format: 
-----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------

****************************************************************************************************/
#define CMD_REC_SMS_BY_INDEX    "AT+CMGR="


//init mms
#define CMD_MMS_INIT    "AT+CMMSINIT\r"
#define CMD_MMS_URL     "AT+CMMSCURL="
#define CMD_MMS_CID     "AT+CMMSCID="
#define CMD_MMS_PROTO   "AT+CMMSPROTO="
#define CMD_MMS_SENDCFG "AT+CMMSSENDCFG="

//activate mms
#define CMD_MMS_SAPBR   "AT+SAPBR="

//edit mms
#define CMD_MMS_EDIT    "AT+CMMSEDIT="
#define CMD_MMS_DOWN    "AT+CMMSDOWN="
#define CMD_MMS_RECP    "AT+CMMSRECP="
#define CMD_MMS_CC      "AT+CMMSCC="
#define CMD_MMS_BCC     "AT+CMMSBCC="
#define CMD_MMS_DELCC   "AT+CMMSDELCC"
#define CMD_MMS_DELBCC  "AT+CMMSDELBCC"

#define CMD_MMS_VIEW    "AT+CMMSVIEW"
#define CMD_MMS_DELFILE "AT+CMMSDELFILE="

#define CMD_MMS_SEND    "AT+CMMSSEND\r"
#define CMD_MMS_TERM    "AT+CMMSTERM\r"


//email
#define CMD_EMAIL_PAYLOAD1   "AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r"
#define CMD_EMAIL_PAYLOAD2   "AT+SAPBR=3,1,\"APN\",\"CMNET\"\r"
#define CMD_EMAIL_ACTIVE_GPRS   "AT+SAPBR=1,1\r"
#define CMD_EMAIL_QUERY      "AT+SAPBR=2,1\r"
#define CMD_EMAIL_CLOSE      "AT+SAPBR=0,1\r"
#define CMD_EMAIL_SETCID     "AT+EMAILCID=1\r"
#define CMD_EMAIL_TIMEOUT    "AT+EMAILTO=30\r"
#define CMD_EMAIL_SVRADDR    "AT+SMTPSRV=\"smtp.163.com\",25\r"
#define CMD_EMAIL_AUTH       "AT+SMTPAUTH=1,\"tang_ming_cong@163.com\",\"tmc5660159\"\r"
#define CMD_EMAIL_FROM       "AT+SMTPFROM=\"tang_ming_cong@163.com\",\"thomas\"\r"
#define CMD_EMAIL_TO         "AT+SMTPRCPT=0,0,\"279712317@qq.com\",\"john\"\r"
#define CMD_EMAIL_CC         "AT+SMTPRCPT=1,0,\"john@sim.com\",\"john\"\r"
#define CMD_EMAIL_BCC        "AT+SMTPRCPT=2,0,\"john@sim.com\",\"john\"\r"
#define CMD_EMAIL_SUB        "AT+SMTPSUB=\"Test\"\r"
#define CMD_EMAIL_BL         "AT+SMTPBODY=19\r"
#define CMD_EMAIL_SEND       "AT+SMTPSEND\r"


typedef enum SMS_FORMAT
{
    EN_PDU,
    EN_TEXT
}SMS_FORMAT_ENUM;


typedef struct MMS_SETTING
{
    char mmsc[50];
    char vpnip[15];
    int port;
    char net[15];
}MMS_SETTING;

int sim800l_save_setting(void);
int sim800l_disable_echo(void);
int sim800l_serial_init(uint br);
int sim800l_dial_number( char *number );
int sim800l_hangup(void);
int sim800l_send_sms(SMS_FORMAT_ENUM format, char* number, char *msg);
int sim800l_get_sms(int idx, RECVPDU_STRU* revpdu);
int sim800l_listen_sms(void);
#if 0
int sim800l_mms_parament_init(MMS_SETTING *pmmssetting);
#endif

int sim800l_send_mms(MMS_SETTING *pmmssetting, char *file, char *discribe, char *numlist[], int num );

void sim800l_close(void);

#endif /* __ATCMD_H__ */

