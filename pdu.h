/*
SMS Server Tools 3
Copyright (C) 2006- Keijo Kasvi
http://smstools3.kekekasvi.com/

Based on SMS Server Tools 2 from Stefan Frings
http://www.meinemullemaus.de/
SMS Server Tools version 2 and below are Copyright (C) Stefan Frings.

This program is free software unless you got it under another license directly
from the author. You can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation.
Either version 2 of the License, or (at your option) any later version.
*/

#ifndef PDU_H
#define PDU_H

#define PDU_DBG 0

#define SWAP_HALF_BYTE(n)    ((n) = ((n) << 4) | ((n) >> 4) )


#define SIZE_WARNING_HEADERS 4096

//Alphabet values: -1=GSM 0=ISO 1=binary 2=UCS2

#define NF_UNKNOWN          0x81
#define NF_INTERNATIONAL 0x91
#define NF_NATIONAL          0xA1


typedef enum CODED
{
    EN_7BIT = 0x0,
    EN_8BIT = 0x1,
    EN_UCS2 = 0x2,
    EN_REVERSE = 0x3
}CODED_ENUM;

typedef struct ADDR
{
    unsigned char addr_length;
    unsigned char addr_type;
    unsigned char addr_num[25];
}ADDR_STRU;

typedef struct SCTS
{
    unsigned char year;
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char min;
    unsigned char sec;
    unsigned char timezone;
}SCTS_STRU;

typedef struct SSMS_INFO
{
    unsigned char reference;
    unsigned char total;
    unsigned char index;
}SSMS_INFO_STRU;

typedef union IED
{
    SSMS_INFO_STRU ssms;
    unsigned char ied[4];
}IED_UNION;

typedef struct UDH
{
    unsigned char udhl;
    unsigned char iei;
    unsigned char iedl;
    IED_UNION ied;
}UDH_STRU;

typedef struct MSG
{
     unsigned char content_len;
     unsigned char msg[281];
}MSG_STRU;

typedef struct UD
{
    UDH_STRU udh;
    //MSG_STRU msg;
    unsigned char msg[281];
}UD_STRU;

typedef struct RFO
{
    unsigned char rfomti:2;
    unsigned char rfomms:1;
    unsigned char rforeverse:2;
    unsigned char rfosri:1;
    unsigned char rfoudhi:1;
    unsigned char rforp:1;
}RFO_STRU;

typedef struct SFO
{
    unsigned char sfomti:2;
    unsigned char sford:1;
    unsigned char sfovpf:2;
    unsigned char sfossr:1;
    unsigned char sfoudhi:1;
    unsigned char sforp:1;
}SFO_STRU;

typedef union FO
{
    SFO_STRU sfo;
    RFO_STRU rfo;
}FO_UNION;

typedef struct DCS
{
    unsigned char dsc_class:2;
    unsigned char dsc_coded:2;
    unsigned char dsc_ib1b0:1;
    unsigned char dsc_compressed:1;
    unsigned char dsc_reverse:2;
}DCS_STRU;

typedef struct RECVPDU
{
    ADDR_STRU sca;
    FO_UNION fo;
    ADDR_STRU oa;
    unsigned char pid;
    DCS_STRU dcs;
    SCTS_STRU scts;
    unsigned char udl;
    UD_STRU ud;
}RECVPDU_STRU;

char* make_ud(char *msg, int utf8size, char* buf);
int make_pdu(char * number, int number_type, char * message, int alphabet, int validity, char * pdu, char *ud, int seglen);
int parser_pdu(const char * pdu, RECVPDU_STRU *revsms);
int gsmDecode7bit(const unsigned char* pSrc, char* pDst, int nSrcLength);
int gsmEncode7bit(unsigned char* pDst,const char* pSrc);


#endif
