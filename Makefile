#CROSS_COMPILE=arm-hisiv100nptl-linux-
AR		= $(CROSS_COMPILE)ar
AS		= $(CROSS_COMPILE)as
CC		= $(CROSS_COMPILE)gcc
CPP		= $(CROSS_COMPILE)g++
LD		= $(CROSS_COMPILE)ld
NM		= $(CROSS_COMPILE)nm
OBJCOPY	= $(CROSS_COMPILE)objcopy
OBJDUMP	= $(CROSS_COMPILE)objdump
RANLIB	= $(CROSS_COMPILE)ranlib
SIZE	= $(CROSS_COMPILE)size
STRIP	= $(CROSS_COMPILE)strip

CFLAGS+=-g

DRV_SRC:=
DRV_SRC+= $(wildcard *.c)
OBJ:=
OBJ+=$(DRV_SRC:.c=.o)
all:main

main:$(OBJ)
	$(CC) $(OBJ) -o main

#$(OBJ):%.o:%.c
sinclude $(DRV_SRC:.c=.d)

%.d: %.c
	@set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $< > $@.; \
	sed 's,/($*/)/.o[ :]*,/1.o $@ : ,g' < $@. > $@; \
	rm -f $@.

.PHONY:clean
clean:
	@-rm $(OBJ) main *.d  -f

