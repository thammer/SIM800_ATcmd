#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "pdu.h"
#include "atcmd.h" 
#include "type.h"

int main ( int argc, char *argv[] )
{    
    int ret = -1;
    int smsindex = -1;
    RECVPDU_STRU recv;
    MMS_SETTING mmscfg;
    char *numlist[5] = {
        "13670042463",
    };
    
    memset(&mmscfg, 0x0, sizeof(mmscfg));
    memcpy(mmscfg.mmsc,"mmsc.myuni.com.cn",sizeof(mmscfg.mmsc));
    memcpy(mmscfg.vpnip,"10.0.0.172", sizeof(mmscfg.vpnip));
    mmscfg.port = 80;
    memcpy(mmscfg.net,"3gwap", sizeof(mmscfg.net));
    
#if 1    
    ret = sim800l_serial_init(115200);
    if ( ret < 0 )
    {
        printf("init seial port error\n");
        return -1;
    }
    
    //sim800l_send_sms(EN_TEXT, "13670042463", argv[1]);
    //sim800l_send_sms(EN_PDU, "13670042463", argv[1]);
    //ret = sim800l_mms_parament_init(&mmscfg);
    //ret = sim800l_send_mms(&mmscfg, "./a.jpg","door opened", numlist, 1);

    //sim800l_dial_number("1008611");
    //sleep(5);
    //sim800l_hangup();
    //sim800l_send_sms(EN_PDU, "10086", "BJ");
    #if 1
    while (1)
    {
        smsindex = sim800l_listen_sms();
        printf("smsindex:%d\n",smsindex);
        memset(&recv, 0x0, sizeof(recv));
        ret = sim800l_get_sms(smsindex, &recv);
        printf("ret:%d\n",ret);
    }
    #endif
    
    sim800l_close();
#else
    parser_pdu( pdu, &recv);
#endif
    return 0;
} 
